import { ScriptTypeBase, createScript } from '../../lib/create-script-decorator';

@createScript()
export abstract class HitableBase extends ScriptTypeBase implements Hitable {
    name = 'hitableBase'

    discriminator(): string {
        return "HITABLE"
    }
    abstract onHit();
}

export interface Hitable extends ScriptType {
    discriminator(): string
    onHit(): void
}

export function isHitable(obj: any): obj is Hitable {
    const discriminator = obj.discriminator
    if (!discriminator) return false
    return discriminator() === 'HITABLE'
}

