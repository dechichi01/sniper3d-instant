import { createScript } from "../../lib/create-script-decorator";
import { UIProgressBar } from "../ui/UIProgressBar";
import { LevelController } from '../game/LevelController';
import { FPCameraController } from './FPCameraController';
import { SniperApp } from '../game/App';

@createScript()
export class UIZoomDrag extends UIProgressBar implements ScriptType {
    [fire: string]: any
    name = 'uiZoomDrag'

    // initialize code called once per entity
    initialize() {
        super.initialize()
        if (!this.app.touch) {
            SniperApp.LevelController().CameraController().on(FPCameraController.EVENT_ZOOM_CHANGED, this.onZoomChanged, this)
        }
    }

    onZoomChanged(zoomValue: number) {
        this.progress = zoomValue
        this.updateUI()
    }
}