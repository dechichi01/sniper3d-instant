import { createScript, ScriptTypeBase } from '../../../lib/create-script-decorator';
import { Hitable, HitableBase } from '../Hitable';
@createScript()
export class RigidObjectHitable extends HitableBase {
    [fire: string]: any
    name = 'rigidObjectHitable'

    onHit() {
        console.log('Hit rigid object!')
    }
}