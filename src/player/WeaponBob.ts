import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { PCUtils, Vec3Util, QuaternionUtil } from '../utils/utils';
import { Transform } from '../utils/transform';
import { Weapon } from './Weapon';
import { FPCameraController } from './FPCameraController';

@createScript()
export class WeaponBob extends ScriptTypeBase implements ScriptType {
    name = 'weaponBob'

    public Aiming: boolean

    private parentTarget: Transform
    private lastParentForward: pc.Vec3
    private startLocalPos: pc.Vec3
    private weapon: Weapon

    private bobSensitivity: pc.Vec2
    private bobSmoothness: number
    private positionBobSensitivity: pc.Vec2
    private positionBobSmoothness: number
    private aimSpeed: number
    private weaponEntity: pc.Entity
    private cameraEntity: pc.Entity

    attributes = {
        bobSensitivity: {
            type: 'vec2',
            default: [400, 400],
        } as pc.AttributesArgs,
        bobSmoothness: {
            type: 'number',
            default: 10,
        } as pc.AttributesArgs,
        positionBobSensitivity: {
            type: 'vec2',
            default: [0.5, 0.1],
        } as pc.AttributesArgs,
        positionBobSmoothness: {
            type: 'number',
            default: 2,
        } as pc.AttributesArgs,
        aimSpeed: {
            type: 'number',
            default: 10,
        } as pc.AttributesArgs,
        weaponEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        cameraEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
    }
    // initialize code called once per entity
    initialize() {
        this.parentTarget = new Transform(this.entity.parent)
        this.lastParentForward = this.parentTarget.Node().forward.clone()
        this.startLocalPos = this.entity.getLocalPosition().clone()

        this.weapon = this.weaponEntity.script['weapon']
        this.cameraEntity.script['fpCameraController'].on(FPCameraController.EVENT_ZOOM_TOGGLE, (isAiming) => this.Aiming = isAiming)
    }

    // update is called every frame
    async update(dt: number) {
        this.updateBob(dt)
    }

    private updateBob(dt: number) {
        if (!this.Aiming && dt != 0) {
            const speedMultiplied = this.positionBobSmoothness
            //TODO if reloading

            const parentNode = this.parentTarget.Node()
            const delta = parentNode.forward.clone().sub(this.lastParentForward).scale(1 / (dt * 60))
            this.lastParentForward = parentNode.forward.clone()
            const localDelta = this.parentTarget.inverseTransformDirection(delta)

            const newLocalPos = Vec3Util.lerp(
                this.entity.getLocalPosition(),
                new pc.Vec3(localDelta.x * this.positionBobSensitivity.x, localDelta.y * this.positionBobSensitivity.y, 0).add(this.startLocalPos),
                speedMultiplied * dt)

            const newLocalEulers = Vec3Util.lerp(
                this.entity.getLocalEulerAngles(),
                new pc.Vec3(localDelta.y * this.bobSensitivity.y, -localDelta.x * this.bobSensitivity.x, 0),
                this.bobSmoothness * dt
            )

            this.entity.setLocalPosition(newLocalPos)
            this.entity.setLocalEulerAngles(new pc.Vec3(newLocalEulers.x, newLocalEulers.y, newLocalEulers.z))
        } else {
            const scopeEntity = this.weapon.Scope()
            const scope = scopeEntity ? this.parentTarget.inverseTransformPoint(this.weapon.Scope().getPosition()) : new pc.Vec3(0, 0, 0)
            const targetLocalPos = Vec3Util.lerp(this.entity.getLocalPosition(), scope.scale(-1).add(this.entity.getLocalPosition()), this.bobSmoothness * dt)
            //Todo: change to smoothdamp -> transform.localPosition = Vector3.SmoothDamp (transform.localPosition, -scope + transform.localPosition, ref _aimVelocity, 1.0f / _aimSpeed);
            const targetLocalRot = QuaternionUtil.lerp(this.entity.getLocalRotation(), pc.Quat.IDENTITY.clone(), this.bobSmoothness * dt)

            this.entity.setLocalPosition(targetLocalPos)
            this.entity.setLocalRotation(targetLocalRot)
        }
    }

}