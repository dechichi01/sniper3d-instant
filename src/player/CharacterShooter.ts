import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { FPCameraController } from './FPCameraController';
import { SniperApp } from '../game/App';
import { CameraUtils } from '../utils/utils';
import { Weapon } from './Weapon';
import { PlayerInputDetector } from './PlayerInputDetector';

@createScript()
export class CharacterShooter extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'characterShooter'

    private cameraController: FPCameraController
    private screen: pc.ScreenComponent

    private weapon: Weapon

    private playerInputDetectEntity: pc.Entity
    private weaponEntity: pc.Entity
    attributes = {
        playerInputDetectEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        weaponEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
    }

    // initialize code called once per entity
    initialize() {
        this.cameraController = SniperApp.LevelController().CameraController()
        this.screen = SniperApp.GameHUD().Screen()
        this.weapon = this.weaponEntity.script['weapon']

        const inputDetector = this.playerInputDetectEntity.script['playerInputDetector']
        inputDetector.on(PlayerInputDetector.SHOT_INPUT, this.shoot, this)
    }

    private shoot() {
        if (!this.cameraController.IsAiming() || !this.weapon.CanShoot()) {
            return
        }

        this.performShoot()
    }

    private async performShoot() {
        const hit = this.shootTest()

        if (hit && hit.hitable) {
            hit.hitable.onHit()
        }
    }

    private shootTest() {
        const camera = this.cameraController.entity.camera
        const start = this.cameraController.entity.getPosition()
        const end = camera.screenToWorld(
            this.screen.resolution.x / 2, this.screen.resolution.y / 2, camera.farClip
        )

        return this.weapon.shootTest(start, end)
    }
}