import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { Hitable, isHitable } from './Hitable';

@createScript()
export class Weapon extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'weapon'

    public static EVENT_SHOT = 'shotEvent'
    //TODO: LOad all this from weapon data
    private maxAmmo: number = 10
    private fireRate: number = 1

    //
    private currentAmmo: number
    private coolingDown: boolean

    private rbSystem: pc.RigidBodyComponentSystem

    private scopeEntity: pc.Entity

    attributes = {
        scopeEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
    }

    public Scope() {
        return this.scopeEntity
    }

    public CanShoot(): boolean {
        return this.currentAmmo > 0 && !this.coolingDown
    }

    initialize() {
        this.rbSystem = this.app.systems['rigidbody']
        this.initParams()
    }

    private initParams() {
        this.currentAmmo = this.maxAmmo
        this.coolingDown = false
    }

    public shootTest(from: pc.Vec3, to: pc.Vec3) {
        if (!this.CanShoot()) {
            return null
        }

        this.fire(Weapon.EVENT_SHOT)

        //this.currentAmmo = Math.max(0, this.currentAmmo - 1)
        this.coolingDown = true
        setTimeout(() => {
            this.coolingDown = false
        }, 1000 / this.fireRate);

        const hit = this.rbSystem.raycastFirst(from, to)
        if (hit) {
            return new HitInfo(hit)
        }

        return null
    }

    public reload() {
        this.currentAmmo = this.maxAmmo
    }

}

@createScript()
export class HitInfo implements ScriptType {
    name = "hitInfo"

    public hitable: Hitable
    public hitResult: pc.RaycastResult

    constructor(result: pc.RaycastResult) {
        if (!result) return
        this.hitResult = result
        const hitable = Object.values(result.entity.script).find(v => isHitable(v))
        this.hitable = hitable
    }
}