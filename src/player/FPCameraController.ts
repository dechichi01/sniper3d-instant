import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { PlayerInputDetector } from './PlayerInputDetector';
import { LevelController } from '../game/LevelController';
import { MathUtil, Vec2Util } from '../utils/utils';
import { CrossHairController } from './CrossHairController';
import { GameHUD } from '../game/GameHUD';
import { SniperApp } from '../game/App';

/*
TODOS:
. Add instability
*/
@createScript()
export class FPCameraController extends ScriptTypeBase implements ScriptType {
    [fire: string]: any;

    public static EVENT_ZOOM_TOGGLE: string = 'onZoomToggle'
    public static EVENT_ZOOM_CHANGED: string = 'zoomChanged'

    name = 'fpCameraController'

    //attributes
    private movementAcceleration: number
    private aimSensitivity: number
    private zoomSensitivityPower: number
    private minAngleRot: pc.Vec2
    private maxAngleRot: pc.Vec2
    private fovChangeSpeed: number

    private crossHairController: CrossHairController

    attributes = {
        movementAcceleration: {
            type: 'number',
            default: 20,
        } as pc.AttributesArgs,
        aimSensitivity: {
            type: 'number',
            default: 0.5,
        } as pc.AttributesArgs,
        zoomSensitivityPower: {
            type: 'number',
            default: 0.5,
        } as pc.AttributesArgs,
        minAngleRot: {
            type: 'vec2',
            default: [-180, -60]
        } as pc.AttributesArgs,
        maxAngleRot: {
            type: 'vec2',
            default: [180, 60]
        } as pc.AttributesArgs,
        fovChangeSpeed: {
            type: 'number',
            default: 15,
        } as pc.AttributesArgs
    }

    //
    private camera: pc.CameraComponent
    private inputDetect: PlayerInputDetector

    private startingAngle: pc.Vec2
    private angle: pc.Vec2
    private targetAngle: pc.Vec2
    private inputDelta: pc.Vec2

    private zoomPercent: number = 0
    private startingZoom: number
    private currentZoom: number
    private lastZoom: number

    private fovCurrentSpeed: number

    public IsAiming(): boolean {
        return this.currentZoom != 1
    }

    // initialize code called once per entity
    initialize() {
        this.camera = this.entity.camera
        this.inputDetect = this.entity.script['playerInputDetector']
        this.crossHairController = SniperApp.GameHUD().CrossHairController()

        //Angle
        const startingAngle = this.entity.getLocalEulerAngles()
        this.startingAngle = new pc.Vec2(-startingAngle.y, -startingAngle.x)
        this.angle = this.startingAngle.clone()
        this.targetAngle = this.angle.clone()

        //Zoom
        this.startingZoom = Math.tan(this.camera.fov * MathUtil.Deg2Rad / 2)
        this.currentZoom = 1
        this.lastZoom = 1

        this.inputDelta = new pc.Vec2(0, 0)
        this.currInputDelta = this.inputDelta.clone()

        this.inputDetect.on(PlayerInputDetector.AIM_MOVE, this.onInputMoved, this)
        this.inputDetect.on(PlayerInputDetector.CHANGE_ZOOM, this.onZoomInputChanged, this)
    }

    // update is called every frame
    update(dt) {
        this.updateMovement(dt)
        this.updateFOVWithZoom(dt)
    }

    private updateMovement(dt) {
        this.targetAngle.add(this.inputDelta.scale(dt))

        this.targetAngle = simplifyAngle(this.targetAngle)
        this.targetAngle = clampAngle(this.targetAngle, this.minAngleRot, this.maxAngleRot)

        this.angle = Vec2Util.lerp(this.angle, this.targetAngle, dt * this.movementAcceleration)

        this.entity.setLocalEulerAngles(-this.angle.y, -this.angle.x, 0)
    }

    private updateFOVWithZoom(dt: number) {
        this.currentZoom = this.getZoomValue()
        const targetFOV = 2 * Math.atan(this.startingZoom / (this.currentZoom * LevelController.ZoomMultiplier)) * MathUtil.Rad2Deg

        const smoothObj = MathUtil.smoothDamp(this.camera.fov, targetFOV, this.fovCurrentSpeed,
            1 / this.fovChangeSpeed, 100000, dt)

        this.fovCurrentSpeed = smoothObj.currentVelocity
        this.camera.fov = smoothObj.result

        if (this.currentZoom == 1 && this.lastZoom > 1 || (this.currentZoom > 1 && this.lastZoom == 1)) {
            this.toggleZoomUI()
        }

        this.lastZoom = this.currentZoom
    }

    private async toggleZoomUI() {
        const isAiming: boolean = this.IsAiming()
        this.fire(FPCameraController.EVENT_ZOOM_TOGGLE, isAiming)
        await this.crossHairController.toggleZoomUI(isAiming)
        this.camera.nearClip = isAiming ? 2 : 0.01
        //TODO: Play audio
    }

    private onInputMoved(delta: pc.Vec2) {
        delta.scale(this.getExtraSensitivity())

        this.inputDelta.x = delta.x;
        this.inputDelta.y = delta.y;
    }

    private onZoomInputChanged(delta: number) {
        this.zoomPercent = MathUtil.clamp(this.zoomPercent + delta, 0, 1)
        this.fire(FPCameraController.EVENT_ZOOM_CHANGED, this.zoomPercent)
    }

    private getExtraSensitivity() {
        return (this.currentZoom > 1 ? this.aimSensitivity : 1) * Math.pow(this.currentZoom, this.zoomSensitivityPower)
    }

    getZoomValue() {
        if (this.zoomPercent <= 0.05) {
            return 1
        }

        return 2 * Math.exp(Math.log(LevelController.MaxZoom / 2) * this.zoomPercent)
    }
}

//Private methods
function simplifyAngle(angle: pc.Vec2) {
    const simplifyAngleValue = (a: number) => {
        while (a < -180) a += 360;
        while (a > 180) a -= 360;

        return a;
    };

    return new pc.Vec2(simplifyAngleValue(angle.x), simplifyAngleValue(angle.y));
}

function clampAngle(angle: pc.Vec2, minAngle: pc.Vec2, maxAngle: pc.Vec2) {
    return new pc.Vec2(MathUtil.clamp(angle.x, minAngle.x, maxAngle.x),
        MathUtil.clamp(angle.y, minAngle.y, maxAngle.y));
}