import { createScript, ScriptTypeBase } from "../../lib/create-script-decorator";
import { FPCameraController } from './FPCameraController';
import { SniperUtils } from '../utils/utils';
import { UIKeepSquared } from '../ui/UIKeepSquared';
import { UIAnimation } from '../ui/UIAnimation';
import { SniperApp } from '../game/App';
import { Weapon } from './Weapon';

@createScript()
export class CrossHairController extends ScriptTypeBase implements ScriptType {
    name = 'crossHairController'

    private crossHairEntity: pc.Entity
    private zoomCrossHairEntity: pc.Entity
    private zoomCHKeepSquaredEntity: pc.Entity

    private zoomCHKeepSquared: UIKeepSquared
    private zoomCHUIAnimation: UIAnimation

    private aimDelay: number

    attributes = {
        crossHairEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        zoomCrossHairEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        zoomCHKeepSquaredEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        aimDelay: {
            type: 'number',
            default: 0.25,
        } as pc.AttributesArgs,
    }

    initialize() {
        this.zoomCHKeepSquared = this.zoomCHKeepSquaredEntity.script['uiKeepSquared']
        this.zoomCHUIAnimation = this.zoomCrossHairEntity.script['uiAnimation']
        SniperApp.LevelController().PlayerWeapon().on(Weapon.EVENT_SHOT, this.onWeaponShot, this)
        this.setCrossHairUI(false)
    }

    public async toggleZoomUI(isAiming: boolean) {
        const delay: number = isAiming ? this.aimDelay : 0
        await SniperUtils.sleepAsync(delay)
        this.setCrossHairUI(isAiming)
    }

    private setCrossHairUI(aiming: boolean) {
        this.crossHairEntity.enabled = !aiming
        this.zoomCrossHairEntity.enabled = aiming

        if (aiming) {
            this.zoomCHKeepSquared.keepSquared()
        }
    }

    private onWeaponShot() {
        this.zoomCHUIAnimation.play()
    }
}