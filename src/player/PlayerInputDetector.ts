import { createScript, ScriptTypeBase } from "../../lib/create-script-decorator";
import { DragDetector } from '../modules/mobile_input/DragDetector';
import { GameHUD } from '../game/GameHUD';
import { UIProgressBar } from "../ui/UIProgressBar";
import { MouseInputDetector } from '../modules/pc_input/MouseInputDetector';
import { SniperApp } from "../game/App";
import { TouchDetector } from '../modules/mobile_input/TouchDetector';
import { TouchButton } from '../modules/mobile_input/TouchButton';

@createScript()
export class PlayerInputDetector extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'playerInputDetector'

    private static MAX_TOUCH_MAGNITUDE = 4

    public static AIM_MOVE = 'aimMoved'
    public static CHANGE_ZOOM = 'changeZoom'
    public static SHOT_INPUT = 'shotInput'

    private mouseSensitivity: number
    private touchSensitivity: number
    private accelerometerSensitivityPower: number
    private accelerometerLinearity: number
    private sensitivityMultiplier: number = 1

    attributes = {
        mouseSensitivity: {
            type: 'number',
            default: 20,
        } as pc.AttributesArgs,
        touchSensitivity: {
            type: 'number',
            default: 100,
        } as pc.AttributesArgs,
        accelerometerSensitivityPower: {
            type: 'number',
            default: 2,
        } as pc.AttributesArgs,
        accelerometerLinearity: {
            type: 'number',
            default: 0.125,
        } as pc.AttributesArgs,
    }

    // initialize code called once per entity
    initialize() {
        if (this.app.touch) {//TODO: Refactor touch detector
            const gameHud = SniperApp.GameHUD()

            const dragDetect = DragDetector.newDragDetector(this.entity, gameHud.AimArea())
            dragDetect.on(DragDetector.EVENT_DRAG, this.onTouchDrag, this)

            const shotDetect = TouchButton.newTouchButton(this.entity, gameHud.ShotButton())
            shotDetect.on(TouchButton.EVENT_TAP, this.onShotInput, this)

            gameHud.ZoomProgressBar().on(UIProgressBar.FILL_PROGRESS_CHANGE, this.onZoomProgressBarChanged, this)
        } else {
            this.entity.script.create('mouseInputDetector')

            var mouseInputDetector = this.entity.script['mouseInputDetector']
            mouseInputDetector.on(MouseInputDetector.EVENT_MOUSE_MOVE, this.onInputMoved, this)
            mouseInputDetector.on(MouseInputDetector.EVENT_MOUSE_CLICK, this.onShotInput, this)
            mouseInputDetector.on(MouseInputDetector.EVENT_MOUSE_WHEEL, this.onMouseDrag, this)
        }
    }

    onInputMoved(pos: pc.Vec2, deltaPos: pc.Vec2) {
        const sensitivity = this.mouseSensitivity * this.sensitivityMultiplier;
        const mouseDelta = new pc.Vec2(deltaPos.x * sensitivity, deltaPos.y * sensitivity)
        this.fire(PlayerInputDetector.AIM_MOVE, mouseDelta);
    }

    onShotInput(pos: pc.Vec2) {
        this.fire(PlayerInputDetector.SHOT_INPUT)
    }

    onMouseDrag(deltaWheel: number) {
        this.fire(PlayerInputDetector.CHANGE_ZOOM, deltaWheel / 5)
    }

    //Touch
    onTouchDrag(delta: pc.Vec2) {
        const magnitude = Math.min(delta.length(), PlayerInputDetector.MAX_TOUCH_MAGNITUDE)
        const normalized = delta.normalize()

        const sensitivity = this.touchSensitivity * this.sensitivityMultiplier;
        let touchDelta = normalized.scale(Math.pow(magnitude, this.accelerometerSensitivityPower)).add(delta.scale(this.accelerometerLinearity))
        touchDelta.scale(sensitivity)

        this.fire(PlayerInputDetector.AIM_MOVE, touchDelta)
    }

    onZoomProgressBarChanged(delta: number) {
        this.fire(PlayerInputDetector.CHANGE_ZOOM, delta)
    }
}