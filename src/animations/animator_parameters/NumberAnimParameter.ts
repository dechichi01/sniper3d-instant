import { createScript } from "../../../lib/create-script-decorator";
import { AnimatorParameter } from './AnimatorParameter';

@createScript()
export class NumberAnimParameter extends AnimatorParameter {
    name = 'numberAnimParameter'

    constructor(name: string) {
        super(name)
        this.currValue = 0
    }

    checkSatisfied(condition: string): boolean {
        throw new Error("Method not implemented.");
    }
}