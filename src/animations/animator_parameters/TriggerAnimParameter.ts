import { createScript } from "../../../lib/create-script-decorator";
import { AnimatorParameter } from "./AnimatorParameter";

@createScript()
export class TriggerAnimParameter extends AnimatorParameter {
    name = 'triggerAnimParameter'

    constructor(name: string) {
        super(name)
        this.currValue = false
    }

    checkSatisfied(condition: string): boolean {
        const result = <boolean>this.currValue
        this.currValue = false
        return result
    }
}