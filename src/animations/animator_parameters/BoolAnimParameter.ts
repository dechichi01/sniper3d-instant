import { createScript } from "../../../lib/create-script-decorator";
import { AnimatorParameter } from "./AnimatorParameter";

@createScript()
export class BoolAnimParameter extends AnimatorParameter {
    name = 'boolAnimParameter'

    constructor(name: string) {
        super(name)
        this.currValue = false
    }

    checkSatisfied(condition: string): boolean {
        const bCondition: boolean = condition.toLowerCase() === 'true'
        return <boolean>this.currValue === bCondition
    }
}