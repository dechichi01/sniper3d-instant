import { createScript } from "../../../lib/create-script-decorator";

@createScript()
export abstract class AnimatorParameter implements ScriptType {
    name = 'animatorParameterBase'

    protected currValue: any

    constructor(name: string) {
        this.name = name
    }

    setValue(value: any) {
        this.currValue = value
    }

    abstract checkSatisfied(condition: string): boolean
}