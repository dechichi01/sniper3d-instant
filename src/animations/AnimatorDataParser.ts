import { createScript } from "../../lib/create-script-decorator";
import { AnimatorState } from './AnimatorState';
import { AnimatorParameter } from './animator_parameters/AnimatorParameter';
import { AnimatorTransition } from './AnimatorTransition';
import { BoolAnimParameter } from './animator_parameters/BoolAnimParameter';
import { TriggerAnimParameter } from "./animator_parameters/TriggerAnimParameter";
import { NumberAnimParameter } from './animator_parameters/NumberAnimParameter';

@createScript()
export class AnimatorDataParser implements ScriptType {
    name = 'animatorDataParser'

    private static parametersGenerator: Map<string, (name: string) => AnimatorParameter> = new Map([
        ['number', (name: string) => new NumberAnimParameter(name)],
        ['boolean', (name: string) => new BoolAnimParameter(name)],
        ['trigger', (name: string) => new TriggerAnimParameter(name)]
    ])

    public static parseAnimatorData(data: object): AnimatorData {
        //Parameters
        const parametersData: object = data['parameters']
        const parameters: Map<string, AnimatorParameter> = new Map

        Object.keys(parametersData).forEach(key => {
            const names: Array<string> = parametersData[key]
            const generator = this.parametersGenerator.get(key)
            names.forEach(name => {
                parameters.set(name, generator(name))
            });
        });

        //States
        const statesData: object = data['states']
        const statesNames = Object.keys(statesData)
        const statesMap: Map<string, AnimatorState> = new Map

        statesNames.forEach(name => {
            const stateData = statesData[name]
            statesMap.set(name, AnimatorState.FromData(name, stateData))
        });

        const states: AnimatorState[] = Array.from(statesMap.values())
        const defaultStateName: string = data['defaultState']
        const defaultState: AnimatorState = statesMap.get(defaultStateName)

        //Transitions
        const transitionsData: Array<object> = data['transitions']
        transitionsData.forEach(data => {
            const fromState = statesMap.get(data['fromState'])
            const toState = statesMap.get(data['toState'])

            const transition = AnimatorTransition.FromData(fromState, toState, parameters, data)
            fromState.addOutTransition(transition)
        });

        //Any state transitions
        const anyStateTransitionsData: Array<object> = data['anyStateTransitions']
        const anyStateTransitions: AnimatorTransition[] = new Array(anyStateTransitionsData.length)
        anyStateTransitionsData.forEach((data, i) => {
            const fromState = statesMap.get(data['fromState'])
            const toState = statesMap.get(data['toState'])

            const transition = AnimatorTransition.FromData(fromState, toState, parameters, data)
            anyStateTransitions[i] = transition
        });

        if (!states || !defaultState || !parameters || !anyStateTransitions) {
            throw new Error(`Invalid parameters on AnimatorDataParsers: \n
                states: ${states}, defaultState: ${defaultState}, parameters: ${parameters}, anyStateTransitions: ${anyStateTransitions}`)
        }

        return new AnimatorData(states, defaultState, parameters, anyStateTransitions)
    }
}

@createScript()
export class AnimatorData implements ScriptType {
    name = 'animatorData'

    public states: AnimatorState[]
    public defaultState: AnimatorState
    public allParameters: Map<string, AnimatorParameter> = new Map
    public anyStateTransitions: AnimatorTransition[]

    constructor(states: AnimatorState[], defaultState: AnimatorState,
        allParameters: Map<string, AnimatorParameter>, anyStateTransitions: AnimatorTransition[]) {

        this.states = states
        this.defaultState = defaultState
        this.allParameters = allParameters
        this.anyStateTransitions = anyStateTransitions
    }
}