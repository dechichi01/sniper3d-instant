import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { AnimatorState } from './AnimatorState';
import { AnimatorParameter } from './animator_parameters/AnimatorParameter';
import { AnimatorTransition } from './AnimatorTransition';
import { AnimatorData, AnimatorDataParser } from './AnimatorDataParser';

@createScript()
export class AnimatorController extends ScriptTypeBase implements ScriptType {
    name = 'animatorController'

    private animatorData: AnimatorData
    private currentState: AnimatorState

    private anim: pc.AnimationComponent
    private animatorDataAsset: pc.Asset

    attributes = {
        animatorDataAsset: {
            type: 'asset',
            assetType: 'json',
        } as pc.AttributesArgs,
    }

    // initialize code called once per entity
    initialize() {
        this.anim = this.entity.animation
        this.parseAnimatorData(this.animatorDataAsset.resource)
    }

    private parseAnimatorData(data: object) {
        this.animatorData = AnimatorDataParser.parseAnimatorData(data)
    }

    public setParameter(name: string, value: any) {
        const parameter = this.animatorData.allParameters.get(name)
        if (!parameter) {//TODO: Type checking
            return
        }

        parameter.setValue(value)

        this.checkStateForTransition()
    }

    private checkStateForTransition() {
        let transition = AnimatorState.getSatisfiedTransition(this.animatorData.anyStateTransitions)
        if (!transition) {
            transition = this.currentState.getSatisfiedTransition()
            if (!transition) {
                return
            }
        }

        this.currentState = transition.executeTransition(this.anim)
    }
}