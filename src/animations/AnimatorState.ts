import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { AnimatorTransition } from './AnimatorTransition';

@createScript()
export class AnimatorState implements ScriptType {
    name = 'animatorState'

    stateName: string
    private animationName: string
    private loop: boolean
    private speed: number
    private outTransitions: AnimatorTransition[]

    constructor(name: string, animationName: string, loop: boolean, speed: number) {
        this.stateName = name
        this.animationName = animationName
        this.loop = loop
        this.speed = speed
        this.outTransitions = []
    }

    public addOutTransition(transition: AnimatorTransition) {
        this.outTransitions.push(transition)
    }

    public static FromData(name: string, data: object): AnimatorState {
        const animName: string = data['animationName']
        const loop: boolean = data['loop']
        const speed: number = data['speed']

        if (!name || !animName || loop == undefined || !speed) {
            throw new Error(`Missing parameters: Name: ${name}, "\n Data: ${JSON.stringify(data)}`)
        }

        return new AnimatorState(name, animName, loop, speed)
    }

    public static getSatisfiedTransition(transitions: AnimatorTransition[]): AnimatorTransition {
        let result: AnimatorTransition = null

        for (let i = 0; i < transitions.length; i++) {
            const transition = transitions[i];
            const satisfied = transition.checkSatisfied()
            if (!result) {
                result = transition
            }
        }

        return result
    }

    getSatisfiedTransition(): AnimatorTransition {
        return AnimatorState.getSatisfiedTransition(this.outTransitions)
    }

    play(anim: pc.AnimationComponent) {
        anim.play(this.animationName, 0.5)//TODO: Blend on transition
        anim.loop = this.loop
    }
}