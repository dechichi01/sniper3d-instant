import { createScript } from "../../lib/create-script-decorator";
import { AnimatorState } from './AnimatorState';
import { AnimatorParameter } from './animator_parameters/AnimatorParameter';

@createScript()
export class AnimatorTransition implements ScriptType {
    name = 'animatorTransition'

    private fromState: AnimatorState
    private toState: AnimatorState
    private hasExitTime: boolean
    private blendTime: number
    private parameters: Map<AnimatorParameter, string>

    constructor(fromState: AnimatorState, toState: AnimatorState, hasExitTime: boolean,
        blendTime: number, parameters: Map<AnimatorParameter, string>) {
        this.fromState = fromState
        this.toState = toState
        this.hasExitTime = hasExitTime
        this.blendTime = blendTime
        this.parameters = parameters
    }

    public static FromData(fromState: AnimatorState, toState: AnimatorState,
        parametersMap: Map<string, AnimatorParameter>, data: object): AnimatorTransition {

        const hasExitTime: boolean = data['hasExitTime']
        const blendTime: number = data['blendTime']
        const parameters: object = data['parameters']

        if (!toState || !blendTime || !parameters) {
            throw new Error(`Missing arguments on AnimatorTransition build: ${toState}\n${JSON.stringify(data)}`)
        }

        const parametersToCondition: Map<AnimatorParameter, string> = new Map

        Object.keys(parameters).forEach(paramName => {
            const parameter: AnimatorParameter = parametersMap.get(paramName)
            if (!parameter) {
                throw new Error(`Paramter with name ${paramName} missing in parameters`)
            }
            const condition: string = parameters[name]
            parametersToCondition.set(parameter, condition)
        })

        return new AnimatorTransition(fromState, toState, hasExitTime, blendTime, parametersToCondition)
    }

    checkSatisfied(): boolean {
        const keys = Array.from(this.parameters.keys())
        for (const parameter of keys) {
            const condition = this.parameters.get(parameter)
            if (!parameter.checkSatisfied(condition)) {
                return false
            }
        }
        return true
    }

    executeTransition(anim: pc.AnimationComponent): AnimatorState {
        this.toState.play(anim)
        return this.toState
    }
}