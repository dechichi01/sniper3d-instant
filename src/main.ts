// !!!REQUIRED IMPORTS!!!


/**********************************************/
/* Useful PlayCanvas polyfills and extensions */
/**********************************************/




/**********************************************/
/* Your code                                  */
/**********************************************/
import './utils/utils'

import './game/App'
//Ui
import './ui/UIProgressBar'
import './ui/UIKeepSquared'
import './ui/UIAnimation'

import './game/GameHUD'
import './player/PlayerInputDetector'
import './player/FPCameraController'
import './player/CrossHairController'
import './player/WeaponBob'
import './player/Weapon'
import './player/CharacterShooter'
import './player/UIZoomDrag'

import './player/shooting/RigidObjectHitable'

import './modules/pc_input/MouseInputDetector'
import './modules/mobile_input/DragDetector'

import './animations/AnimatorController'