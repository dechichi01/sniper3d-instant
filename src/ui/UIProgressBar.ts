import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { FingerDetector } from '../modules/mobile_input/FingerDetector';
import { FingerTouch } from '../modules/mobile_input/FingerTouch';
import { MathUtil, UIUtils } from '../utils/utils';
import { GameHUD } from '../game/GameHUD';

@createScript()
export class UIProgressBar extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'uiProgressBar'

    public static FILL_PROGRESS_CHANGE: string = 'fillProgressChange'
    private static FILL_DIR_HORIZONTAL: number = 0
    private static FILL_DIR_VERTICAL: number = 1

    protected progress: number
    private progressOnTouchStart: number
    private progressElMaxScreenDimension: number
    private anchorMin: number
    private anchorMax: number

    private inputDetectionElement: pc.ElementComponent
    private progressBarElement: pc.ElementComponent

    private inputDetectionEntity: pc.Entity
    private progressBarEntity: pc.Entity
    private startValue: number
    private fillDirection: number
    private reverseFill: boolean

    attributes = {
        inputDetectionEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        progressBarEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        startValue: {
            type: 'number',
            default: 1,
        } as pc.AttributesArgs,
        fillDirection: {
            type: 'number',
            default: 0,
            enum: [
                { 'horizontal': UIProgressBar.FILL_DIR_HORIZONTAL },
                { 'vertical': UIProgressBar.FILL_DIR_VERTICAL }
            ]
        } as pc.AttributesArgs,
        reverseFill: {
            type: 'boolean',
            default: false,
        } as pc.AttributesArgs,
    }

    // initialize code called once per entity
    initialize() {
        this.inputDetectionElement = this.inputDetectionEntity.element
        this.progressBarElement = this.progressBarEntity.element

        if (this.fillDirection == UIProgressBar.FILL_DIR_HORIZONTAL) {
            this.anchorMin = this.progressBarElement.anchor.x
            this.anchorMax = this.progressBarElement.anchor.z
            this.progressElMaxScreenDimension = UIUtils.getScreenDimensions(this.progressBarElement).width
        } else {
            this.anchorMin = this.progressBarElement.anchor.y
            this.anchorMax = this.progressBarElement.anchor.w
            this.progressElMaxScreenDimension = UIUtils.getScreenDimensions(this.progressBarElement).height
        }

        this.setProgress(this.startValue)

        if (this.app.touch) {
            const fingerDetector = FingerDetector.newFingerDetector(this.entity, this.inputDetectionElement)

            fingerDetector.on(FingerDetector.FINGER_MOVED, this.onFingerMoved, this)
            fingerDetector.on(FingerDetector.FINGER_DOWN, this.onFingerDown, this)
        }
    }

    public setProgress(value: number) {
        value = pc.math.clamp(value, 0, 1)
        const delta = this.progress - value
        this.progress = value
        this.updateUI()

        const sign = this.fillDirection == UIProgressBar.FILL_DIR_HORIZONTAL ? 1 : -1

        this.fire(UIProgressBar.FILL_PROGRESS_CHANGE, sign * delta)
    }

    protected updateUI() {
        const value = this.progress

        const { min, max } = this.getAnchorBounds()
        const anchorValue = pc.math.lerp(min, max, value)
        if (this.fillDirection == UIProgressBar.FILL_DIR_HORIZONTAL) {
            this.reverseFill ? this.progressBarElement.anchor.x = anchorValue : this.progressBarElement.anchor.z = anchorValue
        } else {
            this.reverseFill ? this.progressBarElement.anchor.y = anchorValue : this.progressBarElement.anchor.w = anchorValue
        }

        this.progressBarElement.anchor = this.progressBarElement.anchor

        if (this.progressBarElement.rect) {//If it's an image
            this.progressBarElement.rect.z = value//So the rect only shows part of the element component
            this.progressBarElement.rect = this.progressBarElement.rect
        }
    }

    private getAnchorBounds() {
        return this.reverseFill ? { min: this.anchorMax, max: this.anchorMin } : { min: this.anchorMin, max: this.anchorMax }
    }

    private onFingerDown(finger: FingerTouch) {
        this.progressOnTouchStart = this.progress
    }
    private onFingerMoved(finger: FingerTouch) {
        const delta = this.fillDirection == UIProgressBar.FILL_DIR_HORIZONTAL ?
            finger.currPos.x - finger.startPos.x : finger.startPos.y - finger.currPos.y

        const progress = delta / this.progressElMaxScreenDimension + this.progressOnTouchStart
        this.setProgress(progress)
    }
}