import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { GameHUD } from '../game/GameHUD';
import { UIUtils, RectUtils } from '../utils/utils';

@createScript()
export class UIKeepSquared extends ScriptTypeBase implements ScriptType {
    name = 'uiKeepSquared'

    private canvasReferenceEntity: pc.Entity
    private sqrRatio: number

    attributes = {
        canvasReferenceEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        sqrRatio: {
            type: 'number',
            default: 1.5,
        } as pc.AttributesArgs,
    }

    private Element(): pc.ElementComponent {
        return this.entity.element
    }

    public keepSquared() {
        const canvasReference = this.canvasReferenceEntity.element
        const canvasWidth = canvasReference.width
        const canvasHeight = canvasReference.height

        const screenAspect = canvasWidth / canvasHeight

        const element = this.Element()
        if (screenAspect < 1) {
            const desiredNormalizedWidth = (this.sqrRatio / screenAspect)
            element.width = desiredNormalizedWidth * canvasWidth
        } else {
            const desiredNormalizedHeight = screenAspect / this.sqrRatio
            element.height = desiredNormalizedHeight * canvasHeight
        }

        element.anchor = element.anchor
    }

}