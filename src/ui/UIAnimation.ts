import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { SniperUtils, MathUtil } from '../utils/utils';
import { AnimatorState } from '../animations/AnimatorState';

@createScript()
export class UIAnimation extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'uiAnimation'

    private positionAnim: CurveAnimation
    private rotationAnim: CurveAnimation
    private scaleAnim: CurveAnimation

    private positionCurve: pc.CurveSet
    private positionAnimDuration: number
    private rotationCurve: pc.CurveSet
    private rotationAnimDuration: number
    private scaleCurve: pc.CurveSet
    private scaleAnimDuration: number

    attributes = {
        positionCurve: {
            type: 'curve',
            title: "Position Curve",
            curves: ['x', 'y'],
        } as pc.AttributesArgs,
        positionAnimDuration: {
            type: 'number',
            default: 1,
        } as pc.AttributesArgs,
        rotationCurve: {
            type: 'curve',
            title: 'Rotatation Curve',
            curves: ['x', 'y', 'z'],
        } as pc.AttributesArgs,
        rotationAnimDuration: {
            type: 'number',
            default: 1,
        } as pc.AttributesArgs,
        scaleCurve: {
            type: 'curve',
            title: 'Scale Curve',
            curves: ['s'],
        } as pc.AttributesArgs,
        scaleAnimDuration: {
            type: 'number',
            default: 1,
        } as pc.AttributesArgs,
    }

    // initialize code called once per entity
    initialize() {
        const posAnimState = new CurveAnimStateVec3(this.entity.getLocalPosition(), false)
        this.positionAnim = new CurveAnimation(this.positionAnimDuration, false, this.positionCurve, posAnimState,
            (animState) => this.entity.setLocalPosition(animState.getCurrentValue())
        )

        const rotAnimState = new CurveAnimStateVec3(this.entity.getLocalEulerAngles(), false)
        this.rotationAnim = new CurveAnimation(this.rotationAnimDuration, false, this.rotationCurve, rotAnimState,
            (animState) => this.entity.setLocalEulerAngles(animState.getCurrentValue())
        )

        const scaleAnimState = new CurveAnimStateVec3(this.entity.getLocalScale(), true)
        this.scaleAnim = new CurveAnimation(this.scaleAnimDuration, false, this.scaleCurve, scaleAnimState,
            (animState) => this.entity.setLocalScale(animState.getCurrentValue())
        )
    }

    // update is called every frame
    update(dt: number) {
        if (this.positionAnim.isPlaying) {
            this.positionAnim.execAnim(dt)
        }

        if (this.rotationAnim.isPlaying) {
            this.rotationAnim.execAnim(dt)
        }

        if (this.scaleAnim.isPlaying) {
            this.scaleAnim.execAnim(dt)
        }
    }

    public play() {
        this.positionAnim.isPlaying = true
        this.rotationAnim.isPlaying = true
        this.scaleAnim.isPlaying = true
    }
}

@createScript()
class CurveAnimation implements ScriptType {
    name = 'curveAnimation'

    public dt: number = 0
    public loop: boolean = false

    private curve: pc.CurveSet
    private curveAnimState: CurveAnimationState
    private onAnimated: (animState: CurveAnimationState) => void

    public isPlaying: boolean = false
    private percent: number = 0
    public duration: number = 1

    constructor(duration: number, loop: boolean, curve: pc.CurveSet, curveAnimState: CurveAnimationState,
        onAnimated: (animState: CurveAnimationState) => void) {
        this.loop = loop
        this.duration = duration
        this.curve = curve
        this.curveAnimState = curveAnimState
        this.onAnimated = onAnimated
    }

    public execAnim(dt: number) {
        this.isPlaying = true
        this.dt = dt
        this.percent = MathUtil.clamp(this.percent + dt * 1 / this.duration, 0, 1)

        const curveValue = this.curve.value(this.percent)
        this.curveAnimState.updateFromCurveValue(curveValue)
        this.onAnimated(this.curveAnimState)

        if (this.percent >= 1) {
            this.percent = 0
            this.isPlaying = false
        }
    }
}

interface CurveAnimationState extends ScriptType {
    updateFromCurveValue(curveValue: any[]): void
    getCurrentValue<T>(): T
}

@createScript()
class CurveAnimStateVec3 implements CurveAnimationState {
    name = 'curveAnimStateVec3'

    private startVec: pc.Vec3
    private currVec: pc.Vec3

    private updateUniform: boolean

    constructor(startVec: pc.Vec3, uniform: boolean) {
        if (!startVec) return

        this.startVec = startVec.clone()
        this.currVec = this.startVec.clone()
        this.updateUniform = uniform
    }

    updateFromCurveValue(curveValue: any[]): void {
        let curveDelta: pc.Vec3

        const firstCurveValue = isNaN(curveValue[0]) ? curveValue : curveValue[0]
        const defaultDelta = this.updateUniform ? firstCurveValue : 0

        curveDelta = new pc.Vec3(firstCurveValue, curveValue[1] || defaultDelta, curveValue[2] || defaultDelta)

        this.currVec = this.startVec.clone().add(curveDelta)
    }

    getCurrentValue(): any {
        return this.currVec.clone()
    }
}

