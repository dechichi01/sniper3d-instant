import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { GameHUD } from './GameHUD';
import { LevelController } from './LevelController';

@createScript()
export class SniperApp extends ScriptTypeBase implements ScriptType {
    name = 'sniperApp'

    private static _instance: SniperApp

    private gameHUD: pc.Entity
    private levelController: pc.Entity

    attributes = {
        gameHUD: {
            type: 'entity',
        } as pc.AttributesArgs,
        levelController: {
            type: 'entity',
        } as pc.AttributesArgs,
    }


    public static GameHUD() { return <GameHUD>SniperApp._instance.gameHUD.script['gameHUD'] }
    public static LevelController() { return <LevelController>SniperApp._instance.levelController.script['levelController'] }

    initialize() {
        if (SniperApp._instance && SniperApp._instance !== this) {
            this.entity.destroy()
            return
        }

        SniperApp._instance = this
    }

}