import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { UIProgressBar } from '../ui/UIProgressBar';
import { CrossHairController } from '../player/CrossHairController';

@createScript()
export class GameHUD extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'gameHUD'

    private zoomAreaEntity: pc.Entity
    private aimAreaEntity: pc.Entity
    private shotButtonEntity: pc.Entity
    private reloadButtonEntity: pc.Entity
    private screenEntity: pc.Entity
    private zoomProgressBarEntity: pc.Entity
    private crossHairControllerEntity: pc.Entity

    attributes = {
        zoomAreaEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        aimAreaEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        shotButtonEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        reloadButtonEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        screenEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        zoomProgressBarEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        crossHairControllerEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
    }

    public ZoomArea() { return this.zoomAreaEntity.element }
    public AimArea() { return this.aimAreaEntity.element }
    public ShotButton() { return this.shotButtonEntity.element }
    public ReloadButton() { return this.reloadButtonEntity.element }
    public Screen() { return this.screenEntity.screen }
    public ZoomProgressBar() { return <UIProgressBar>this.zoomProgressBarEntity.script['uiZoomDrag'] }
    public CrossHairController() { return <CrossHairController>this.crossHairControllerEntity.script['crossHairController'] }
}