import { createScript, ScriptTypeBase } from '../../lib/create-script-decorator';
import { FPCameraController } from '../player/FPCameraController';
import { CharacterShooter } from '../player/CharacterShooter';
import { Weapon } from '../player/Weapon';

@createScript()
export class LevelController extends ScriptTypeBase implements ScriptType {
    name = 'levelController'

    static MaxZoom = 5
    static ZoomMultiplier = 1

    private cameraCtrlEntity: pc.Entity
    private characterShooterEntity: pc.Entity
    private weaponEntity: pc.Entity

    attributes = {
        cameraCtrlEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        characterShooterEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
        weaponEntity: {
            type: 'entity',
        } as pc.AttributesArgs,
    }

    public CameraController() { return <FPCameraController>this.cameraCtrlEntity.script['fpCameraController'] }
    public Player() { return <CharacterShooter>this.characterShooterEntity.script['characterShooter'] }
    public PlayerWeapon() { return <Weapon>this.weaponEntity.script['weapon'] }
}