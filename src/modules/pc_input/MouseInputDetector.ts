import { createScript, ScriptTypeBase } from "../../../lib/create-script-decorator";
import { MathUtil } from '../../utils/utils';

@createScript()
export class MouseInputDetector extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'mouseInputDetector'

    private static MAX_MOUSE_DELTA = 30

    public static EVENT_MOUSE_MOVE = 'mouseMove'
    public static EVENT_MOUSE_CLICK = 'mouseClick'
    public static EVENT_MOUSE_WHEEL = 'mouseWheel'
    // initialize code called once per entity
    initialize() {
        this.app.mouse.disableContextMenu();

        this.app.mouse.on(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
        this.app.mouse.on(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
        this.app.mouse.on(pc.EVENT_MOUSEWHEEL, this.onMouseDrag, this);
    }

    onMouseMove(cursor: pc.MouseEvent) {
        const max = MouseInputDetector.MAX_MOUSE_DELTA

        const pos = new pc.Vec2(cursor.x, cursor.y)
        const deltaPos = new pc.Vec2(MathUtil.clamp(cursor.dx, -max, max), MathUtil.clamp(cursor.dy, -max, max))

        this.fire(MouseInputDetector.EVENT_MOUSE_MOVE, pos, deltaPos)
    }

    onMouseDown(cursor: pc.MouseEvent) {
        const pos = new pc.Vec2(cursor.x, cursor.y)
        this.fire(MouseInputDetector.EVENT_MOUSE_CLICK, pos)
    }

    onMouseDrag(cursor: pc.MouseEvent) {
        this.fire(MouseInputDetector.EVENT_MOUSE_WHEEL, cursor.wheel)
    }
}