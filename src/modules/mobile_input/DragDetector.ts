import { createScript, ScriptTypeBase } from "../../../lib/create-script-decorator";
import { MathUtil, PCUtils } from '../../utils/utils';
import { GameHUD } from '../../game/GameHUD';
import { TouchDetector } from './TouchDetector';
import { FingerTouch } from './FingerTouch';
import { TouchActionDetectorBase } from './DetectorBase';
import { ElementTouchDetector } from './ElementTouchDetector';

@createScript()
export class DragDetector extends ElementTouchDetector implements ScriptType {
    [fire: string]: any
    name = 'dragDetector'

    static EVENT_DRAG: string = 'touchDrag'

    private touchDetector: TouchDetector
    private deltaTime: number

    update(dt: number) {
        this.deltaTime = dt
    }

    public static newDragDetector(entity: pc.Entity, element: pc.ElementComponent): DragDetector {
        return ElementTouchDetector.newElementTouchDetector<DragDetector>(entity, element, 'dragDetector')
    }

    protected subscribeToTouchDetector(touchDetector: TouchDetector) {
        touchDetector.on(TouchDetector.EVENT_TOUCHES_MOVE, this.onTouchMove, this)
    }

    private onTouchMove(changedFingers: FingerTouch[]) {
        changedFingers = this.filterFingers(changedFingers)

        if (changedFingers.length === 0) {
            return
        }

        const touchDelta = new pc.Vec2(0, 0)

        changedFingers.forEach(finger => {
            const delta = finger.currPos.clone().sub(finger.lastPos)
            touchDelta.add(delta)
            //Todo: Screen height
        });

        touchDelta.scale(1 / (this.deltaTime * 60))

        this.fire(DragDetector.EVENT_DRAG, touchDelta)
    }
}