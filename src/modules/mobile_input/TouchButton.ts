import { createScript } from '../../../lib/create-script-decorator';
import { ElementTouchDetector } from './ElementTouchDetector';
import { TouchDetector } from './TouchDetector';
import { FingerTouch } from './FingerTouch';
@createScript()
export class TouchButton extends ElementTouchDetector implements ScriptType {
    [fire: string]: any
    name = 'touchButton'

    public static EVENT_TAP: string = 'touchTap'
    public static EVENT_HOLD: string = 'touchHold'

    public static newTouchButton(entity: pc.Entity, element: pc.ElementComponent): TouchButton {
        return ElementTouchDetector.newElementTouchDetector<TouchButton>(entity, element, 'touchButton')
    }

    protected subscribeToTouchDetector(touchDetector: TouchDetector) {
        touchDetector.on(TouchDetector.EVENT_TOUCH_TAP, this.onTap, this)
    }

    private onTap(finger: FingerTouch) {
        console.log('-> fINGER TAPED -> ', finger)
        if (!this.fingerInsideRect(finger)) {
            return
        }

        this.fire(TouchButton.EVENT_TAP)
    }
}