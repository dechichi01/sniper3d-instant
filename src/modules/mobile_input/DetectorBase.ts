import { createScript, ScriptTypeBase } from "../../../lib/create-script-decorator";
import { TouchDetector } from './TouchDetector';
import { PCUtils } from "../../utils/utils";

@createScript()
export abstract class TouchActionDetectorBase extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'touchActionDetectorBase'

    protected static newActionDetector<T extends TouchActionDetectorBase>(entity: pc.Entity, name: string): T {
        const touchDetector = TouchDetector.Instance()
        const actionDetector = PCUtils.createScript<T>(entity, name)
        actionDetector.setTouchDetector(touchDetector)

        return actionDetector
    }

    public setTouchDetector(touchDetector: TouchDetector) {
        this.touchDetector = touchDetector;

        if (this.app.touch) {
            this.subscribeToTouchDetector(this.touchDetector)
        }
    }

    protected abstract subscribeToTouchDetector(touchDetector: TouchDetector);
}