import { createScript } from "../../../lib/create-script-decorator";
import { TouchActionDetectorBase } from './DetectorBase';
import { FingerTouch } from './FingerTouch';
import { UIUtils, RectUtils } from '../../utils/utils';

@createScript()
export abstract class ElementTouchDetector extends TouchActionDetectorBase implements ScriptType {
    [fire: string]: any
    name = 'elementTouchDetector'

    protected element: pc.ElementComponent

    public static newElementTouchDetector<T extends ElementTouchDetector>(entity: pc.Entity, element: pc.ElementComponent, name: string): T {
        const elementTouchDetector = TouchActionDetectorBase.newActionDetector<T>(entity, name)
        elementTouchDetector.element = element
        return elementTouchDetector
    }

    protected filterFingers(fingers: FingerTouch[]): FingerTouch[] {
        if (!this.element) {
            return fingers
        }

        const elementRect = UIUtils.getScreenRect(this.element)
        return fingers.filter(f => RectUtils.containsPoint(elementRect.left, elementRect.bottom,
            elementRect.right, elementRect.top, f.startPos))
    }

    protected fingerInsideRect(finger: FingerTouch): boolean {
        if (!this.element) {
            return true
        }

        const elementRect = UIUtils.getScreenRect(this.element)
        return RectUtils.containsPoint(elementRect.left, elementRect.bottom,
            elementRect.right, elementRect.top, finger.startPos)
    }
}