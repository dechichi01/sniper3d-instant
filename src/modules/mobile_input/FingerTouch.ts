import { createScript } from "../../../lib/create-script-decorator";

@createScript()
export class FingerTouch {
    name = "fingerTouch"

    id: string
    startPos: pc.Vec2
    currPos: pc.Vec2
    lastPos: pc.Vec2
    startTime: number

    constructor(id: string, startX: number, startY: number, starTime: number) {
        this.id = id
        this.startPos = new pc.Vec2(startX, startY)
        this.currPos = this.startPos.clone()
        this.lastPos = this.startPos.clone()
        this.startTime = this.startTime
    }

    public update(currX: number, currY: number): void {
        this.lastPos = this.currPos.clone()
        this.currPos = new pc.Vec2(currX, currY)
    }

    public snapShot(): FingerTouch {
        const copy = new FingerTouch(this.id, this.startPos.x, this.startPos.y, this.startTime)
        copy.lastPos = this.lastPos.clone()
        copy.currPos = this.currPos.clone()

        return copy
    }
}