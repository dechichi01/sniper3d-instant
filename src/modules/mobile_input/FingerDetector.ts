import { createScript } from "../../../lib/create-script-decorator";
import { TouchActionDetectorBase } from './DetectorBase';
import { TouchDetector } from './TouchDetector';
import { FingerTouch } from './FingerTouch';
import { ElementTouchDetector } from './ElementTouchDetector';

@createScript()
export class FingerDetector extends ElementTouchDetector implements ScriptType {
    [fire: string]: any
    name = 'fingerDetector'

    public static FINGER_DOWN: string = 'fingerDown'
    public static FINGER_MOVED: string = 'fingerMoved'
    public static FINGER_UP: string = 'fingerUp'

    private currFingerId: string

    public static newFingerDetector(entity: pc.Entity, element: pc.ElementComponent): FingerDetector {
        return ElementTouchDetector.newElementTouchDetector<FingerDetector>(entity, element, 'fingerDetector')
    }

    protected subscribeToTouchDetector(touchDetector: TouchDetector) {
        touchDetector.on(TouchDetector.EVENT_TOUCHES_START, this.onTouchStart, this)
        touchDetector.on(TouchDetector.EVENT_TOUCHES_MOVE, this.onTouchMove, this)
        touchDetector.on(TouchDetector.EVENT_TOUCHES_END, this.onTouchEnd, this)
    }

    private onTouchStart(fingers: FingerTouch[]) {
        if (!!this.currFingerId) {
            return
        }

        fingers = this.filterFingers(fingers)

        if (fingers.length === 0) {
            return
        }

        const finger = fingers[0]
        this.currFingerId = finger.id
        this.fire(FingerDetector.FINGER_DOWN, finger)
    }

    private onTouchMove(fingers: FingerTouch[]) {
        const finger: FingerTouch = !this.currFingerId ? null : fingers.find(f => f.id == this.currFingerId)
        if (!finger) {
            return
        }

        this.fire(FingerDetector.FINGER_MOVED, finger)
    }

    private onTouchEnd(fingers: FingerTouch[]) {
        const finger: FingerTouch = !this.currFingerId ? null : fingers.find(f => f.id == this.currFingerId)
        if (!finger) {
            return
        }

        this.currFingerId = null

        this.fire(FingerDetector.FINGER_UP, finger)
    }
}