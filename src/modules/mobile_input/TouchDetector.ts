import { createScript, ScriptTypeBase } from '../../../lib/create-script-decorator';
import { FingerTouch } from './FingerTouch';
import { PCUtils } from '../../utils/utils';

@createScript()
export class TouchDetector extends ScriptTypeBase implements ScriptType {
    [fire: string]: any
    name = 'touchDetector'

    private static _instance: TouchDetector

    static EVENT_TOUCHES_START: string = 'touchStart'
    static EVENT_TOUCHES_MOVE: string = 'touchMove'
    static EVENT_TOUCHES_END: string = 'touchEnd'
    static EVENT_TOUCH_TAP: string = 'touchTap'
    static EVENT_TOUCHES_HOLD: string = 'touchHold'

    private fingers: Map<number, FingerTouch> = new Map()
    private changedFingers: FingerTouch[]

    //attributes
    private holdThreshold: number
    attributes = {
        holdThreshold: {
            type: 'number',
            defaultValue: '0.5'
        } as pc.AttributesArgs,
    }

    public static Instance(): TouchDetector { return TouchDetector._instance }

    // public static newTouchDetector(entity: pc.Entity, element: pc.ElementComponent): TouchDetector {
    //     const touchDetector = PCUtils.createScript<TouchDetector>(entity, 'touchDetector')
    //     touchDetector.setup(element)
    //     return touchDetector
    // }

    initialize() {
        if (TouchDetector._instance && TouchDetector._instance !== this) {
            this.entity.destroy()
            return
        }

        TouchDetector._instance = this
        this.setup()
    }

    public setup() {
        const touch = this.app.touch
        if (!touch) {
            return
        }

        touch.on(pc.EVENT_TOUCHSTART, this.onTouchStart, this)
        touch.on(pc.EVENT_TOUCHMOVE, this.onTouchMove, this)
        touch.on(pc.EVENT_TOUCHEND, this.onTouchEnd, this)
        touch.on(pc.EVENT_TOUCHCANCEL, this.onTouchEnd, this)
    }

    private extractChangedTouchesAndFingers(event: pc.TouchEvent) {
        const changedTouches = Object.values(event.changedTouches)
        const changedFingers = new Array<FingerTouch>(changedTouches.length)

        return { changedTouches, changedFingers }
    }

    private onTouchStart(event: pc.TouchEvent) {
        const { changedTouches, changedFingers } = this.extractChangedTouchesAndFingers(event)

        const currentTime = new Date().getSeconds()
        changedTouches.forEach((touch, i) => {
            const fingerTouch = new FingerTouch(touch.id.toString(), touch.x, touch.y, currentTime)
            changedFingers[i] = fingerTouch.snapShot()
            this.fingers.set(touch.id, fingerTouch)
        })

        this.fire(TouchDetector.EVENT_TOUCHES_START, changedFingers)
    }

    private onTouchMove(event: pc.TouchEvent) {
        const { changedTouches, changedFingers } = this.extractChangedTouchesAndFingers(event)

        changedTouches.forEach((touch, i) => {
            const fingerTouch = this.fingers.get(touch.id)
            if (!fingerTouch) {
                return
            }

            fingerTouch.update(touch.x, touch.y)

            this.fingers.set(touch.id, fingerTouch)

            changedFingers[i] = fingerTouch.snapShot()
        });

        this.fire(TouchDetector.EVENT_TOUCHES_MOVE, changedFingers)
    }

    private onTouchEnd(event: pc.TouchEvent) {
        const { changedTouches, changedFingers } = this.extractChangedTouchesAndFingers(event)

        const currentTime = new Date().getSeconds()
        changedTouches.forEach((touch, i) => {
            const fingerTouch = this.fingers.get(touch.id)
            if (!fingerTouch) {
                return
            }

            this.fingers.delete(touch.id)
            fingerTouch.update(touch.x, touch.y)

            changedFingers[i] = fingerTouch.snapShot()

            if (currentTime - fingerTouch.startTime < this.holdThreshold) {
                this.fire(TouchDetector.EVENT_TOUCH_TAP)
            }
        });

        this.fire(TouchDetector.EVENT_TOUCHES_END, changedFingers)
    }
}