import { createScript } from '../../lib/create-script-decorator';

/**
 * @name Transform
 * @class Represents a transform with more utils then pc.GraphNode
*/
@createScript()
export class Transform implements ScriptType {
    name = 'transform'

    private node: pc.GraphNode
    private currWorldMat: pc.Mat4
    private currLocalMat: pc.Mat4

    private localToWorldMat: pc.Mat4
    private worldToLocalMat: pc.Mat4

    constructor(node: pc.GraphNode) {
        if (!node) return
        this.node = node
        this.currWorldMat = node.getWorldTransform().clone()
        this.currLocalMat = node.getLocalTransform().clone()
        this.localToWorldMat = this.calculateLocalToWorld(this.node)
        this.worldToLocalMat = this.calculateWorldToLocal(this.node)
    }

    public Node(): pc.GraphNode {
        return this.node;
    }

    public transformPoint(vec: pc.Vec3): pc.Vec3 {
        return this.getLocalToWorldMatrix(this.node).transformPoint(vec)
    }

    public inverseTransformPoint(vec: pc.Vec3): pc.Vec3 {
        return this.getWorldToLocalMatrix(this.node).transformPoint(vec)
    }

    public transformDirection(vec: pc.Vec3): pc.Vec3 {
        return this.getLocalToWorldMatrix(this.node).transformVector(vec)
    }

    public inverseTransformDirection(vec: pc.Vec3): pc.Vec3 {
        return this.getWorldToLocalMatrix(this.node).transformVector(vec)
    }

    private getLocalToWorldMatrix(node: pc.GraphNode): pc.Mat4 {
        if (this.checkLocalDirty()) {
            this.localToWorldMat = this.calculateLocalToWorld(node)
        }

        return this.localToWorldMat
    }

    private getWorldToLocalMatrix(node: pc.GraphNode): pc.Mat4 {
        if (this.checkWorldDirty()) {
            this.worldToLocalMat = this.calculateWorldToLocal(node)
        }

        return this.worldToLocalMat
    }

    private calculateLocalToWorld(node: pc.GraphNode): pc.Mat4 {
        if (!node.parent) {
            return node.getLocalTransform().clone()
        } else {
            return this.calculateLocalToWorld(node.parent).clone().mul(node.getLocalTransform())
        }
    }

    private calculateWorldToLocal(node: pc.GraphNode): pc.Mat4 {
        return this.getLocalToWorldMatrix(node).invert()
    }

    private checkWorldDirty(): boolean {
        return true
        // const worldMat = this.node.getWorldTransform()
        // if (!this.currWorldMat.equals(worldMat)) {
        //     this.currWorldMat = worldMat.clone()
        //     return true
        // }

        // return false
    }

    private checkLocalDirty(): boolean {
        return true
        // const localMat = this.node.getLocalTransform()
        // if (!this.currLocalMat.equals(localMat)) {
        //     this.currLocalMat = localMat.clone()
        //     return true
        // }

        // return false
    }
}