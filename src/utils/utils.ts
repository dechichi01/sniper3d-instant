import { createScript } from '../../lib/create-script-decorator';

@createScript()
export class SniperUtils implements ScriptType {
    name = 'sniperUtils'

    static sleepAsync(time: number) {
        return new Promise(resolve => setTimeout(() => {
            resolve()
        }, time))
    }
}

@createScript()
export class MathUtil implements ScriptType {
    name = 'mathUtil'
    static clamp(v: number, min: number, max: number) {
        return Math.min(Math.max(v, min), max)
    }

    static Deg2Rad = Math.PI / 180
    static Rad2Deg = 180 / Math.PI

    static lerp(a: number, b: number, t: number) {
        return (1 - t) * a + t * b
    }

    static inverseLep(a: number, b: number, v: number) {
        return (v - a) / (b - a)
    }

    static smoothDamp(current: number, target: number, currentVelocity: number, smoothTime: number, maxSpeed: number, deltaTime: number) {
        smoothTime = Math.max(0.0001, smoothTime)
        const num = 2 / smoothTime
        const num2 = num * deltaTime
        const num3 = 1 / (1 + num2 + 0.48 * num2 * num2 + 0.235 * num2 * num2 * num2)
        let num4 = current - target
        const num5 = target
        const num6 = maxSpeed * smoothTime

        num4 = MathUtil.clamp(num4, -num6, num6)
        target = current - num4
        const num7 = (currentVelocity + num * num4) * deltaTime
        currentVelocity = (currentVelocity - num * num7) * num3
        let num8 = target + (num4 + num7) * num3

        if (num5 - current > 0 === num8 > num5) {
            num8 = num5
            currentVelocity = (num8 - num5) / deltaTime
        }

        return { currentVelocity, result: num8 }
    }

    static approximately(a: number, b: number) {
        return Math.abs(a - b) < 0.0001
    }
}

@createScript()
export class Vec2Util implements ScriptType {
    name = 'vec2Util'

    static lerp(a: pc.Vec2, b: pc.Vec2, t: number) {
        return new pc.Vec2(MathUtil.lerp(a.x, b.x, t), MathUtil.lerp(a.y, b.y, t))
    }
}

export class Vec3Util implements ScriptType {
    name = 'vec3Util'

    static lerp(a: pc.Vec3, b: pc.Vec3, t: number): pc.Vec3 {
        return new pc.Vec3(
            MathUtil.lerp(a.x, b.x, t),
            MathUtil.lerp(a.y, b.y, t),
            MathUtil.lerp(a.z, b.z, t)
        )
    }
}

export class QuaternionUtil implements ScriptType {
    name = 'quaternionUtil'

    static lerp(a: pc.Quat, b: pc.Quat, t: number): pc.Quat {
        const retVal = new pc.Quat(
            MathUtil.lerp(a.x, b.x, t),
            MathUtil.lerp(a.y, b.y, t),
            MathUtil.lerp(a.z, b.z, t),
            MathUtil.lerp(a.w, b.w, t)
        )

        retVal.normalize()
        return retVal
    }
}

@createScript()
export class PCUtils implements ScriptType {
    name = "pcUtil"

    static createScript<T>(entity: pc.Entity, name: string) {
        entity.script.create(name)
        const scriptComp: T = entity.script[name]
        return scriptComp
    }
}

@createScript()
export class UIUtils implements ScriptType {
    name = "uiUtils"

    static getScreenDimensions(element: pc.ElementComponent) {
        return {
            width: element.screenCorners[2].x - element.screenCorners[0].x,
            height: element.screenCorners[3].y - element.screenCorners[1].y
        }
    }

    static getScreenRect(element: pc.ElementComponent) {
        return {
            left: element.screenCorners[0].x,
            right: element.screenCorners[2].x,
            bottom: element.screenCorners[1].y,
            top: element.screenCorners[3].y,
        }
    }
}

@createScript()
export class RectUtils implements ScriptType {
    name = "rectUtils"

    static getCenter(left: number, bottom: number, right: number, top: number) {
        return new pc.Vec2(left + (right - left) / 2, bottom + (top - bottom) / 2)
    }

    static containsPoint(left: number, bottom: number, right: number, top: number, point: pc.Vec2) {
        return point.x >= left && point.x <= right &&
            point.y >= bottom && point.y <= top
    }
}

@createScript()
export class CameraUtils implements ScriptType {
    name = "cameraUtils"

    static screenPointToRay(camera: pc.CameraComponent, point: pc.Vec2) {
        const start = camera.screenToWorld(point.x, point.y, camera.nearClip)
        const end = camera.screenToWorld(point.x, point.y, camera.farClip)

        return new pc.Ray(start, end.sub(start))
    }
}